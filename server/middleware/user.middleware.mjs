import yup from "yup";

const creationSchema = yup.object().shape({
  first_name: yup.string().matches(/^[a-zA-Z]+$/).min(3).max(10).required(),
  last_name: yup.string().matches(/^[a-zA-Z]+$/).min(3).max(10).required(),
  email: yup.string().email().required(),
  phone: yup.string().matches(/^\d+$/).required(),
});

const updateSchema = yup.object().shape({
  first_name: yup.string().matches(/^[a-zA-Z]+$/).min(3).max(10),
  last_name: yup.string().matches(/^[a-zA-Z]+$/).min(3).max(10),
  email: yup.string().email(),
  phone: yup.string().matches(/^\d+$/),
});

export const validateUserCreation = (req, res, next) => {
  creationSchema
    .validate(req.body)
    .then(() => next())
    .catch(next);
};

export const validateUserUpdate = (req, res, next) => {
  updateSchema
    .validate(req.body)
    .then(() => next())
    .catch((err) => next(err));
};
